package com.java.nirmata;

import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class NirmataSetUp {
	
	public static WebDriver webDriver;
	public static NirmataApplicationProperties  appproperties;
	public static ExtentReports reports;
	public static ExtentTest suiteInfo;
	public static ExtentTest methodInfo;
	public static ExtentTest testInfo;
	public static ExtentHtmlReporter htmlReporter;
	public static String testName;
	
	@BeforeSuite
	@Parameters("browser")
	public void setupReport(String browser,ITestContext ctx) throws MalformedURLException
	{
		String suiteName = ctx.getCurrentXmlTest().getSuite().getName();
		appproperties=new NirmataApplicationProperties();
		htmlReporter= new ExtentHtmlReporter(appproperties.properties.getProperty("ReportDirectory"));
		htmlReporter.loadXMLConfig(new File(System.getProperty("user.dir") +"/src/main/resources/extent-config.xml"));
		htmlReporter.config().setChartVisibilityOnOpen(false);
		
		//htmlReporter.setAppendExisting(true);
		try {
		FileWriter fileWriterTest = new FileWriter("Report/failedMethods.txt",false);
		
		fileWriterTest.close();	
		} catch (Exception e) {
			e.printStackTrace();
			 
		}
		
		try {
			FileWriter fileWriterSubject = new FileWriter("Report/failedSubject.txt",false);
			
			fileWriterSubject.close();	
			} catch (Exception e) {
				e.printStackTrace();
				 
			}
		
		
		
		reports=new ExtentReports();
		reports.setAnalysisStrategy(AnalysisStrategy.TEST);
		reports.attachReporter(htmlReporter);
		//suiteInfo=reports.createTest(suiteName).assignCategory(suiteName);
		
		switch(browser)
		{
			case "Chrome":
				
				
				System.setProperty("webdriver.chrome.driver", appproperties.properties.getProperty("ChromeDriverPath"));
				ChromeOptions Chromeoptions = new ChromeOptions();
				Chromeoptions.addArguments("start-maximized");
				Chromeoptions.addArguments("--disable-infobars");
				Chromeoptions.addArguments("--no-sandbox");
				webDriver= new ChromeDriver(Chromeoptions);
				/*
				Set<Cookie> allCookies = webDriver.manage().getCookies();
				 for(Cookie cookie : allCookies) {
					 webDriver.manage().addCookie(cookie);
			        }
					*/
				webDriver.get(appproperties.getApplicationUrl());
				
				break;
			case "Firefox":
				
				
				System.setProperty("webdriver.gecko.driver", appproperties.properties.getProperty("FireFoxDriverPath"));
				FirefoxOptions Firefoxoptions = new FirefoxOptions();
				webDriver= new FirefoxDriver(Firefoxoptions);
				webDriver.get(appproperties.getApplicationUrl());
				break;
			default:
				break;
		}
		
		
	}
	@AfterSuite
	public void closeReport()
	{
		reports.flush();
		if(webDriver!=null)
		{
		 webDriver.close();
		}
		
	}

	
	
}
