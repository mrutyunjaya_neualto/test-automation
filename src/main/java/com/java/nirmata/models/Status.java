package com.java.nirmata.models;

import java.util.List;

public class Status {
	private String state;
	private List<ContainerStatuses> containerStatuses;
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public List<ContainerStatuses> getContainerStatuses() {
		return containerStatuses;
	}
	public void setContainerStatuses(List<ContainerStatuses> containerStatuses) {
		this.containerStatuses = containerStatuses;
	}
}
