package com.java.nirmata.models;

import java.util.List;

public class Pods {
	private List<Status> status;

	public List<Status> getStatus() {
		return status;
	}

	public void setStatus(List<Status> status) {
		this.status = status;
	}
}
