package com.java.nirmata.models;

import java.util.List;

public class PodsDetailsModel {
	private String state;
	private String run;
	private List<Pods> pods;
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getRun() {
		return run;
	}
	public void setRun(String run) {
		this.run = run;
	}
	public List<Pods> getPods() {
		return pods;
	}
	public void setPods(List<Pods> pods) {
		this.pods = pods;
	}

}
