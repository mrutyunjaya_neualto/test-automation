package com.java.nirmata.environment;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;

public class CreateEvnvironment {
	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	@BeforeClass
	public void setupDriver()
	{
		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	@Test(priority=6,testName= "Click on Environment Menu")
	public void clickEnvironment() throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(webDriver, 200);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("env_menu")));
		wait.until(ExpectedConditions.elementToBeClickable(By.id("env_menu")));
		WebElement clusterMenu=webDriver.findElement(By.id("env_menu"));
		clusterMenu.click(); 
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addEnvironment")));
	}
	@Test(priority=7,testName= "Click on Add Environment",dependsOnMethods= {"clickEnvironment"})
	public void clickAddEnvironment() throws InterruptedException
	{
		WebElement AddClusters=webDriver.findElement(By.id("addEnvironment"));
		AddClusters.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 200);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='modal-dialog']")));
	}
	@Test(priority=8,testName= "Fill Environment Data",dependsOnMethods= {"clickAddEnvironment"})
	@Parameters({"EnvName","ClusterName"})
	public void fillEnvironmentData(String EnvName,String ClusterName) throws InterruptedException
	{
		WebElement EnvironmentName = webDriver.findElement(By.xpath("//*[@id=\"name\"]"));
		EnvironmentName.sendKeys(EnvName);
		
		Select Cluster = new Select(webDriver.findElement(By.name("hostCluster")));
		Cluster.selectByVisibleText(ClusterName);
		
		Select isolationLevel = new Select(webDriver.findElement(By.name("isolationLevel")));
		isolationLevel.selectByValue(appproperties.properties.getProperty("IsolatedLevel"));
		
		webDriver.findElement(By.xpath("//button[@class='btn btn-primary btn-nirmata']")).click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 200);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='modal-dialog']")));
		Thread.sleep(5000);
	}
	@Test(priority=9,testName= "Wait for Environment Create",dependsOnMethods= {"fillEnvironmentData"})
	public void waitForEnvironmentCreate() throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(webDriver, 9000);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("state-popover")));
		boolean status=true;
		int countFailedState=0;
		Date currentDate=new Date();
		do
		{
			webDriver.navigate().refresh();
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("state-popover")));
			WebElement statusText = webDriver.findElement(By.id("state-popover"));
			System.out.println("environment status "+statusText.getText());
			if(statusText.getText().contains("Cluster Connected"))
			{
				status=false;
			}
			else if(statusText.getText().contains("Cluster Not Connected"))
			{
				countFailedState++;
				if(countFailedState > 20)
				{
				System.out.println("env count failed state"+countFailedState);
				status=false;
				Assert.fail("Environment failed : "+statusText.getText());
				}
			}
			else {
				countFailedState=0;
			}
			long diff =  new Date().getTime()  - currentDate.getTime();
			long diffMinutes = diff / (60 * 1000) % 60;
			//System.out.println("Wait Time :"+diffMinutes);
			if(diffMinutes>5)
			{
				status=false;
				Assert.fail("Environment failed : "+statusText.getText());
				
			}
	 
			
		}while(status);
	}
}
