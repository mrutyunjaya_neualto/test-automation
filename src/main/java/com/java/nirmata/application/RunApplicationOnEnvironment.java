package com.java.nirmata.application;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;

public class RunApplicationOnEnvironment {

	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	@BeforeClass
	public void setupDriver()
	{
		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	@Test(priority=6,testName= "Click on Environment Menu")
	public void clickEnvironmentMenu() throws InterruptedException
	{
		WebElement clusterMenu=webDriver.findElement(By.id("env_menu"));
		clusterMenu.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 200);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("environment-info")));
		
	}
	@Test(priority=7,testName= "Click on Specific Environment",dependsOnMethods= {"clickEnvironmentMenu"})
	@Parameters("EnvName")
	public void clickonSpecificEnvironment(String EnvName) throws InterruptedException
	{ 
		WebElement SpecificApp=webDriver.findElement(By.xpath("//*[contains(text(), '"+EnvName+"')]"));
		SpecificApp.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("application-info")));
	}
	@Test(priority=8,testName= "Click on Run Application",dependsOnMethods= {"clickonSpecificEnvironment"})
	@Parameters("EnvName")
	public void clickonRunApllication(String EnvName) throws InterruptedException
	{
		WebElement RunApp=webDriver.findElement(By.id("application-info"));
		RunApp.click();
		String EnvironnmentName="Run the Application in "+EnvName;
		WebDriverWait wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(text(), '"+EnvironnmentName+"')]")));
	}
	@Test(priority=9,testName= "Fill Application Data To Run",dependsOnMethods= {"clickonRunApllication"})
	@Parameters({"RunAppName","AppName","EnvName"})
	public void fillDataToRunApplication(String RunAppName,String AppName,String EnvName) throws InterruptedException
	{
		WebElement runName = webDriver.findElement(By.xpath("//*[@id=\"run\"]"));
		runName.sendKeys(RunAppName);
		
		Select App = new Select(webDriver.findElement(By.id("application")));
		//System.out.println("Name: "+App.getFirstSelectedOption().getText());
		//System.out.println("Name1: "+appproperties.properties.getProperty("EnvName"));
		
		List<WebElement> options =App.getOptions();
		for(int i=0;i<options.size();i++)
		{
			if(options.get(i).getText().equalsIgnoreCase(AppName))
			{
				App.selectByIndex(i);
				break;
			}
		}
		
		webDriver.findElement(By.xpath("//button[@class='btn btn-primary btn-nirmata']")).click();
		String EnvironmentName="Run the Application in "+EnvName;
		boolean status=true;
		do
		{
			if(webDriver.findElements(By.xpath("//*[contains(text(), '"+EnvironmentName+"')]")).size()!=0)
			{
				try
				{
					WebDriverWait wait = new WebDriverWait(webDriver, 60);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(text(), '"+EnvName+"')]")));
					if(webDriver.findElements(By.id("form-errors")).size()!=0)
					{
						wait = new WebDriverWait(webDriver, 60);
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("form-errors")));
						WebElement errorText= webDriver.findElement(By.id("form-errors"));
						if(errorText.getText()!=null && !errorText.getText().trim().equals(""))
						{
							String errorMessage=errorText.getText();
							webDriver.findElement(By.xpath("//button[@class='btn btn-default pull-left']")).click();
							wait = new WebDriverWait(webDriver, 60);
							wait.until(ExpectedConditions.invisibilityOf(webDriver.findElement(By.xpath("//*[@class='modal-backdrop fade in']"))));
							status=false;
							Assert.fail("Failed to run application : "+errorMessage);
						}
					}
				}
				catch(TimeoutException e)
				{
					if(webDriver.findElements(By.xpath("//*[contains(text(), '"+EnvName+"')]")).size()==0)
					{
						status=false;
					}
				}
				
			}
			else
			{
				status=false;
			}
			
		}while(status);
		
		Thread.sleep(5000);
		
	}
	@Test(priority=10,testName= "Wait For Application To Be Run",dependsOnMethods= {"fillDataToRunApplication"})
	public void waitForApplicationRun() throws InterruptedException
	{
		
		WebDriverWait wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("state-popover")));
		boolean status=true;
		int countFailedState=0;
		Date currentDate=new Date();
		do
		{
		    webDriver.navigate().refresh();
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("state-popover")));
			WebElement statusText = webDriver.findElement(By.id("state-popover"));
			if(statusText.getText().contains("Running"))
			{
				status=false;
			}
			else if(statusText.getText().contains("Failed"))
			{
			    countFailedState++;
				if(countFailedState > 20)
				{
				 System.out.println("app count failed state"+countFailedState);
				 status=false;
				 Assert.fail("Application Deploy failed : "+statusText.getText());
				}
			}
			else {
				countFailedState=0;
			}
			long diff =  new Date().getTime()  - currentDate.getTime();
			long diffMinutes = diff / (60 * 1000) % 60;
			//System.out.println("Wait Time For Deploy Application:"+diffMinutes);
			if(diffMinutes>5)
			{
				status=false;
				Assert.fail("Application Deploy failed : "+statusText.getText());
				
			}
			
			
		}while(status);
	}
}
