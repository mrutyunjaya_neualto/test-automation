package com.java.nirmata.application;

import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;

public class DeleteApplication {
	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	@BeforeClass
	public void setupDriver()
	{
		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	@Test(priority=6,testName= "Click on Application Menu")
	public void clickApplicationMenu() throws InterruptedException
	{
		WebElement clusterMenu=webDriver.findElement(By.id("applications_menu"));
		clusterMenu.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOf(webDriver.findElement(By.id("currentCatalog_menu"))));
		
		WebElement application=webDriver.findElement(By.id("currentCatalog_menu"));
		application.click();
		
		wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("addApplication")));
		
	}
	@Test(priority=7,testName= "Click on Specific Application",dependsOnMethods= {"clickApplicationMenu"})
	@Parameters("AppName")
	public void clickonSpecificApplication(String AppName) throws InterruptedException
	{ 
		WebElement SpecificApp=webDriver.findElement(By.xpath("//*[contains(text(), '"+AppName+"')]"));
		SpecificApp.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-action-button")));
	}
	@Test(priority=8,testName= "Click on Action Button",dependsOnMethods= {"clickonSpecificApplication"})
	public void clickonActionButton() throws InterruptedException
	{
		WebElement actionButton=webDriver.findElement(By.id("model-action-button"));
		actionButton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("deleteApplication")));
	}
	@Test(priority=9,testName= "Click on Delete Button",dependsOnMethods= {"clickonActionButton"})
	public void clickonDeleteButton() throws InterruptedException
	{
		WebElement deleteButton=webDriver.findElement(By.id("deleteApplication"));
		deleteButton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='btn btn-danger']")));
	}
	@Test(priority=10,testName= "Delete Application",dependsOnMethods= {"clickonDeleteButton"})
	public void deleteApplication() throws InterruptedException
	{
		WebElement deleteButton=webDriver.findElement(By.xpath("//button[@class='btn btn-danger']"));
		deleteButton.click();
		boolean status=true;
		Date currentDate=new Date();
		do
		{
			if(webDriver.findElements(By.id("addApplication")).size()!=0)
			{
				status=false;
			}
			long diff =  new Date().getTime()  - currentDate.getTime();
			long diffMinutes = diff / (60 * 1000) % 60;
			//System.out.println("Wait Time For Delete Application :"+diffMinutes);
			if(diffMinutes>5)
			{
				status=false;
			}
			Thread.sleep(10000);
			
		}while(status);
		
	}
}
