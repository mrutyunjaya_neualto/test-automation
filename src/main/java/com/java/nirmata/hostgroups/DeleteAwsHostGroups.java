package com.java.nirmata.hostgroups;

import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;
import com.java.nirmata.hostgroups.RunSystemScripts;


public class DeleteAwsHostGroups {
	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	@BeforeClass
	public void setupDriver()
	{
		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	
	@Test(priority=6,testName= "Click on Host Group Menu")
	public void clickHostGroup()
	{
		WebElement HomeGroupMenu=webDriver.findElement(By.id("hostgroups_menu"));
		HomeGroupMenu.click();
		
	}
	@Test(priority=7,dependsOnMethods= {"clickHostGroup"},testName= "Click on Amazon Web Services Menu")
	public void clickOnDirectConnect()
	{
		WebElement MicrosoftAzurebutton=webDriver.findElement(By.id("aws_menu"));
		MicrosoftAzurebutton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));
		
		WebElement awsPage = webDriver.findElement(By.id("model-content-panel-title"));
		String awsPage_title= awsPage.getText();
		Assert.assertEquals(awsPage_title, appproperties.properties.getProperty("awsPageTitle"));
	}
	@Test(priority=8,dependsOnMethods= {"clickOnDirectConnect"},testName= "Host Group Details")
	@Parameters("HostGroupName")
	public void clickForHostGroupDetails(String HostGroupName)
	{
		
		WebElement SpecificHostGroup=webDriver.findElement(By.xpath("//*[contains(text(), '"+HostGroupName+"')]"));
		SpecificHostGroup.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("state-popover")));
	}
	@Test(priority=9,testName= "Click on Setting Button",dependsOnMethods= {"clickForHostGroupDetails"})
	public void clickOnSettingButton()
	{
		WebElement SettingButton=webDriver.findElement(By.id("model-action-button"));
		SettingButton.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("deleteHostGroup")));
	}
	@Test(priority=10,testName= "Click on Delete Button",dependsOnMethods= {"clickOnSettingButton"})
	@Parameters("HostGroupName")
	public void clickOnDeleteButton(String HostGroupName)
	{
		WebElement DeleteButton=webDriver.findElement(By.id("deleteHostGroup"));
		DeleteButton.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='btn btn-danger']")));
 
		//Input AMI ID
		WebElement inputHostGroupName = webDriver.findElement(By.id("name"));
		inputHostGroupName.sendKeys(HostGroupName);  
	}
	@Test(priority=11,testName= "Delete Aws Host Group",dependsOnMethods= {"clickOnDeleteButton"})
	public void deleteHostGroup()
	{
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='btn btn-danger']")));
        webDriver.findElement(By.xpath("//button[@class='btn btn-danger']")).click();
		
	}
 
}
