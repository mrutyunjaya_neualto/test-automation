package com.java.nirmata.hostgroups;


import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import java.io.IOException;
import java.io.InputStream;


public class RunSystemScripts {
	private NirmataApplicationProperties  appproperties=NirmataSetUp.appproperties;;
    public static class Output{
        boolean success;
        int number;
    }

    public  void commonExec( String cmd) throws IOException {
    	String hostname = appproperties.properties.getProperty("HostIP");
        String username = appproperties.properties.getProperty("HostUserName");
        String password =  appproperties.properties.getProperty("HostPassword");
        JSch jsch = new JSch();
        Session session = null;
        //System.out.println("Trying to connect.....");
        try {

            session = jsch.getSession(username, hostname, 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(password);
            session.connect(); 
            
            ChannelExec channel = null;
            channel = (ChannelExec) session.openChannel("exec");

            channel.setCommand(cmd);
            channel.setInputStream(null);

            InputStream in = channel.getInputStream(); // channel.getInputStream();

            channel.connect();
            String ret = getChannelOutput(channel, in);

            channel.disconnect();
            //System.out.println(ret);
            session.disconnect();
        } catch (JSchException e) {
            e.printStackTrace();  
        }
        //System.out.println("Done !!");

    }
    
    private String getChannelOutput(Channel channel, InputStream in) throws IOException{

        byte[] buffer = new byte[1024];
        StringBuilder strBuilder = new StringBuilder();

        String line = "";
        while (true){
            while (in.available() > 0) {
                int i = in.read(buffer, 0, 1024);
                if (i < 0) {
                    break;
                }
                strBuilder.append(new String(buffer, 0, i));
                //System.out.println(line);
            }

            if(line.contains("logout")){
                break;
            }

            if (channel.isClosed()){
                break;
            }
            try {
                Thread.sleep(1000);
            } catch (Exception ee){}
        }

        return strBuilder.toString();   
    }
    
    
    public void runExec(String cmd) throws IOException {
    	commonExec(cmd);

    }

}