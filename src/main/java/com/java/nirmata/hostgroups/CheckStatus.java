package com.java.nirmata.hostgroups;

import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;

public class CheckStatus {

	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	private ExtentTest methodInfo;
	@BeforeMethod
	public void reportStart(Method method)
	{
		String descriptiveTestName=method.getAnnotation(Test.class).testName();
		if(descriptiveTestName!=null && !descriptiveTestName.trim().equals(""))
		{
		   methodInfo=NirmataSetUp.testInfo.createNode(descriptiveTestName).assignCategory("Host Groups");
		}
		else
		{
			 methodInfo=NirmataSetUp.testInfo.createNode(method.getName()).assignCategory("Host Groups");
		}
		NirmataSetUp.methodInfo=this.methodInfo;
	}

	@BeforeClass
	public void setupDriver()
	{
		//System.out.println("Before Class of create Azure host");
		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	
	@Test(priority=6,testName= "Click on Host Group Menu")
	public void clickHostGroup() throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(webDriver, 360);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("hostgroups_menu")));
		
		WebElement HomeGroupMenu=webDriver.findElement(By.id("hostgroups_menu"));
		HomeGroupMenu.click();
		
	}
	
	@Test(priority=7,dependsOnMethods= {"clickHostGroup"},testName= "Click on Amazon Web Services Menu")
	public void clickOnAwsHostGroup()
	{
		WebElement MicrosoftAzurebutton=webDriver.findElement(By.id("aws_menu"));
		MicrosoftAzurebutton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 360);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));
		
		WebElement awsPage = webDriver.findElement(By.id("model-content-panel-title"));
		String awsPage_title= awsPage.getText();
		Assert.assertEquals(awsPage_title, appproperties.properties.getProperty("awsPageTitle"));
	}
	
	
	@Test(priority=8,dependsOnMethods= {"clickOnAwsHostGroup"},testName= "check status")
	@Parameters("HostGroupName")
	public void checkStatus(String HostGroupName) throws InterruptedException
	{ 
		WebDriverWait wait = new WebDriverWait(webDriver, 360);
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(), '"+HostGroupName+"')]")));
		WebElement clickCreatedCard = webDriver.findElement(By.xpath("//span[contains(text(), '"+HostGroupName+"')]"));
		clickCreatedCard.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='status-badge-lg status-green']")));
		int count = 0;
		while(count<2) {
			
			webDriver.navigate().refresh();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='status-badge-lg status-green']")));
			WebElement checkStatus = webDriver.findElement(By.xpath("//span[@class='status-badge-lg status-green']"));
			//System.out.println("hgStatus"+checkStatus.getText());
			if(checkStatus.getText().equals("Connected")) {
				count++;
				System.out.println("hgStatus"+checkStatus.getText());
			}else {
				Assert.fail("Hostgroup connect : "+checkStatus.getText());
			}
		}
		
	}
	
	 
}
