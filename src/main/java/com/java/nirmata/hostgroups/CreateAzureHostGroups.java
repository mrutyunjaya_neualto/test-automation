package com.java.nirmata.hostgroups;

import java.lang.reflect.Method;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;

public class CreateAzureHostGroups {

	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	private ExtentTest methodInfo;
	@BeforeMethod
	public void reportStart(Method method)
	{
		String descriptiveTestName=method.getAnnotation(Test.class).testName();
		if(descriptiveTestName!=null && !descriptiveTestName.trim().equals(""))
		{
		   methodInfo=NirmataSetUp.testInfo.createNode(descriptiveTestName).assignCategory("Host Groups");
		}
		else
		{
			 methodInfo=NirmataSetUp.testInfo.createNode(method.getName()).assignCategory("Host Groups");
		}
		NirmataSetUp.methodInfo=this.methodInfo;
	}

	@BeforeClass
	public void setupDriver()
	{
		//System.out.println("Before Class of create Azure host");
		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	
	@Test(priority=6,testName= "Click on Host Group Menu")
	public void clickHostGroup() throws InterruptedException
	{
		WebElement HomeGroupMenu=webDriver.findElement(By.id("hostgroups_menu"));
		HomeGroupMenu.click();
		
	}
	
	@Test(priority=7,dependsOnMethods= {"clickHostGroup"},testName= "Click on Microsoft Azure Menu")
	public void clickOnMicrosoftAzure()
	{
		WebElement MicrosoftAzurebutton=webDriver.findElement(By.id("azure_menu"));
		MicrosoftAzurebutton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));
		
		WebElement MicrosoftAzurePage = webDriver.findElement(By.id("model-content-panel-title"));
		String MicrosoftAzurePage_title= MicrosoftAzurePage.getText();
		Assert.assertEquals(MicrosoftAzurePage_title, appproperties.properties.getProperty("MicrosoftAzure"));
	}
	
	@Test(priority=8,dependsOnMethods= {"clickOnMicrosoftAzure"},testName= "Click on add host group button")
	public void clickAddHostGroup()
	{
		WebElement AddHostGroupbutton=webDriver.findElement(By.id("addHostGroup"));
		AddHostGroupbutton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("bootstrap-dialog-title")));
		
		WebElement AddHostGroupDialog = webDriver.findElement(By.className("bootstrap-dialog-title"));
		String AddHostGroupDialog_title= AddHostGroupDialog.getText();
		Assert.assertEquals(AddHostGroupDialog_title, appproperties.properties.getProperty("AzurAddHostGroupTitle"));
	}
	
	@Test(priority=9,dependsOnMethods= {"clickAddHostGroup"},testName= "Set data in Host Group tab")
	public void fillinghostGroupTabData()
	{
		webDriver.findElement(By.id("name")).sendKeys("Automate-test-1");
		Select ClouldProvider = new Select(webDriver.findElement(By.name("parent")));
		ClouldProvider.selectByValue("a22d4dd4-2209-46f8-b935-1d6d1d132cf2");
		
		WebElement errorMessage = webDriver.findElement(By.className("help-block"));
		String errorMessagetest= errorMessage.getText();
		
		if(errorMessagetest!=null && !errorMessagetest.equals(""))
		{
			
				Assert.fail(errorMessagetest);
			
		}
		else
		{
			WebElement wizarderror = webDriver.findElement(By.xpath("//div[@class='text-danger text-center']"));
			String wizarderror_text= wizarderror.getText();
			
			if(wizarderror_text!=null && !wizarderror_text.equals(""))
			{
				Assert.fail(wizarderror_text);
			}
			else
			{
				methodInfo.info("Host Group Name:- Automate-test-1");
				methodInfo.info("Clound Provider:- Nirmata-Azure");
				webDriver.findElement(By.xpath("//button[@class='btn btn-next btn-nirmata btn-default btn-primary']")).click();
				
			}
			
		}
		

	}
	
	@Test(priority=10,dependsOnMethods= {"fillinghostGroupTabData"},testName= "Set data in Settings tab")
	public void fillingSettingTabData()
	{
		try
		{
			
			try
			{
				if(webDriver.findElement(By.className("busy-block")).isDisplayed())
				{
					//System.out.println("Here");
					WebDriverWait waitFetchResourceGroup = new WebDriverWait(webDriver, 40);
					waitFetchResourceGroup.until(ExpectedConditions.visibilityOf(webDriver.findElement(By.className("busy-block"))));
				}
			}
			catch(Exception e1)
			{
				methodInfo.info(e1.getMessage());
			}
		
		Select ResourceGroup = new Select(webDriver.findElement(By.name("resourceGroup")));
		ResourceGroup.selectByValue("anzen-cluster-1");
		
		try
		{
		
			if(webDriver.findElement(By.className("busy-block")).isDisplayed())
			{
				//System.out.println("Here 1");
				WebDriverWait wait = new WebDriverWait(webDriver, 40);
				wait.until(ExpectedConditions.visibilityOf(webDriver.findElement(By.className("busy-block"))));
			}
		}
		catch(Exception e2)
		{
			methodInfo.info(e2.getMessage());
		}
		Select SecurityGroup = new Select(webDriver.findElement(By.name("securityGroups")));
		SecurityGroup.selectByValue("anzen-cluster-1-sg");
		
		
		WebElement errorMessage = webDriver.findElement(By.className("help-block"));
		String errorMessagetest= errorMessage.getText();
		
		if(errorMessagetest!=null && !errorMessagetest.equals(""))
		{
			if(errorMessagetest.contains(appproperties.properties.getProperty("SecurityGroupErrorMessage")))
			{
				Assert.fail(errorMessagetest);
			}
			else
			{
				Assert.fail(errorMessagetest);
			}
			
		}
		else
		{
			methodInfo.info("Resource Group:- anzen-cluster-1 \\(centralus\\)");
			methodInfo.info("Security Group:- anzen-cluster-1-sg");
			
			webDriver.findElement(By.xpath("//button[@class='btn btn-next btn-nirmata btn-default btn-primary']")).click();
		}
		
		}
		catch (Exception e)
		{
			Assert.fail(e.getMessage());
		}
	}
	
	@Test(priority=11,dependsOnMethods= {"fillingSettingTabData"},testName= "Set data in Host Count tab")
	public void fillingHostcountTabData()
	{
		
		methodInfo.info("Taking Default data only.");
		webDriver.findElement(By.xpath("//button[@class='btn btn-next btn-nirmata btn-default btn-primary']")).click();
	}
	
	@Test(priority=12,dependsOnMethods= {"fillingHostcountTabData"},testName= "Set data in User Data tab")
	public void fillingUserDataTabData()
	{
		
		Select UserDataTemplate = new Select(webDriver.findElement(By.name("installSelect")));
		UserDataTemplate.selectByValue("centos");
		
		WebElement FinishButton = webDriver.findElement(By.xpath("//button[@class='btn btn-finish btn-nirmata btn-default btn-primary']"));
		FinishButton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOf(webDriver.findElement(By.xpath("//span[@class='bootstrap-dialog-button-icon fa fa-circle-o-notch fa-spin icon-spin']"))));
		
		WebElement wizarderror = webDriver.findElement(By.xpath("//div[@class='text-danger text-center']"));
		String wizarderror_text= wizarderror.getText();
		
		if(wizarderror_text!=null && !wizarderror_text.equals(""))
		{
			Assert.fail(wizarderror_text);
		}
		
	}
	
}
