package com.java.nirmata.hostgroups;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;

public class CreateHostGroupUsingWebClientStart {
	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	@BeforeClass
	public void setupDriver()
	{
		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	@Test(priority=6,testName= "Open Webclient Start page")
	public void openWebClientStartPage()
	{
		WebElement HomeGroupMenu=webDriver.findElement(By.id("hostgroups_menu"));
		HomeGroupMenu.click();
		
	}
}
