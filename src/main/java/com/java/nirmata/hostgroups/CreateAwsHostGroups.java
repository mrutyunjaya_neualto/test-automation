package com.java.nirmata.hostgroups;

import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.annotations.Parameters;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;

public class CreateAwsHostGroups {

	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	private ExtentTest methodInfo;
	@BeforeMethod
	public void reportStart(Method method)
	{
		String descriptiveTestName=method.getAnnotation(Test.class).testName();
		if(descriptiveTestName!=null && !descriptiveTestName.trim().equals(""))
		{
		   methodInfo=NirmataSetUp.testInfo.createNode(descriptiveTestName).assignCategory("Host Groups");
		}
		else
		{
			 methodInfo=NirmataSetUp.testInfo.createNode(method.getName()).assignCategory("Host Groups");
		}
		NirmataSetUp.methodInfo=this.methodInfo;
	}

	@BeforeClass
	public void setupDriver()
	{ 
		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	
	@Test(priority=6,testName= "Click on Host Group Menu")
	public void clickHostGroup() throws InterruptedException
	{
		WebElement HomeGroupMenu=webDriver.findElement(By.id("hostgroups_menu"));
		HomeGroupMenu.click();
		
	}
	
	@Test(priority=7,dependsOnMethods= {"clickHostGroup"},testName= "Click on Amazon Web Services Menu")
	public void clickOnAwsHostGroup()
	{
		WebElement MicrosoftAzurebutton=webDriver.findElement(By.id("aws_menu"));
		MicrosoftAzurebutton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));
		
		WebElement awsPage = webDriver.findElement(By.id("model-content-panel-title"));
		String awsPage_title= awsPage.getText();
		Assert.assertEquals(awsPage_title, appproperties.properties.getProperty("awsPageTitle"));
	}
	
	@Test(priority=8,dependsOnMethods= {"clickOnAwsHostGroup"},testName= "Click on add host group button")
	public void clickAddHostGroup()
	{
		WebElement AddHostGroupbutton=webDriver.findElement(By.id("addHostGroup"));
		AddHostGroupbutton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("bootstrap-dialog-title")));
		
		WebElement AddHostGroupDialog = webDriver.findElement(By.className("bootstrap-dialog-title"));
		String AddHostGroupDialog_title= AddHostGroupDialog.getText();
		Assert.assertEquals(AddHostGroupDialog_title, appproperties.properties.getProperty("awsAddHostGroupTitle"));
	}
		
	@Test(priority=9,dependsOnMethods= {"clickAddHostGroup"},testName= "Set data in Host Group tab")
    @Parameters({"providerid","HostGroupName"})
	public void fillinghostGroupTabData(String providerid,String HostGroupName)
	{
		webDriver.findElement(By.id("name")).sendKeys(HostGroupName);
		Select ClouldProvider = new Select(webDriver.findElement(By.name("parent"))); 
		ClouldProvider.selectByValue(providerid);
		webDriver.findElement(By.xpath("//button[@class='btn btn-next btn-nirmata btn-default btn-primary']")).click();
	}
	
	@Test(priority=10,dependsOnMethods= {"fillinghostGroupTabData"},testName= "Set data in Settings tab")
	@Parameters("ami")
	public void fillingSettingTabData(String ami)
	{
		try
		{
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
	    
		//select Region
		Select Region = new Select(webDriver.findElement(By.id("region")));
		Region.selectByValue("us-west-1");		
		
		//Select config type
		Select HostInstance = new Select(webDriver.findElement(By.id("awsConfigType")));
		HostInstance.selectByValue("ami");
		
		//Input AMI ID
		WebElement amiId = webDriver.findElement(By.id("image"));
		boolean flag = amiId.isDisplayed();
		Assert.assertTrue(flag);
        amiId.sendKeys(ami);  
        Thread.sleep(10000);
	
        
		//Select Instance Type
		WebElement selectElement = webDriver.findElement(By.xpath("//select[@id=\"instanceType\"]")); 
        Select select= new Select(selectElement);
        wait.until(
        	    ExpectedConditions
        	        .presenceOfNestedElementsLocatedBy(By.xpath("//select[@id=\"instanceType\"]"), By.tagName("option"))
        	);
        wait.until(
        	    ExpectedConditions
        	        .elementToBeClickable(By.xpath("//select[@id=\"instanceType\"]"))
        	);
		List<WebElement> dropdown = select.getOptions();
		Thread.sleep(3000);
		//select.selectByIndex(5);   
		select.selectByValue("c5.large");

    	//Select Keypair
    	WebElement ssh = webDriver.findElement(By.xpath("//select[@id=\"keypair\"]")); 
        Select selectssh= new Select(ssh);
        wait.until(
        	    ExpectedConditions
        	        .presenceOfNestedElementsLocatedBy(By.xpath("//select[@id=\"keypair\"]"), By.tagName("option"))
        	);
        wait.until(
        	    ExpectedConditions
        	        .elementToBeClickable(By.xpath("//select[@id=\"keypair\"]"))
        	);
		List<WebElement> dropdown1 = selectssh.getOptions();
		Thread.sleep(3000);
		selectssh.selectByValue("nirmata-west-1-071814");;

        JavascriptExecutor je = (JavascriptExecutor) webDriver;
        WebElement modal = webDriver.findElement(By.xpath("//select[@id=\"keypair\"]")); 
        je.executeScript("arguments[0].scrollIntoView(true);",modal); 
		 
        //Security group
        WebElement security = webDriver.findElement(By.xpath("//select[@id=\"securityGroups\"]"));
        Select selectsecurity= new Select(security);
        wait.until(
        	    ExpectedConditions
        	        .presenceOfNestedElementsLocatedBy(By.xpath("//select[@id=\"securityGroups\"]"), By.tagName("option"))
        	);
        wait.until(
        	    ExpectedConditions
        	        .elementToBeClickable(By.xpath("//select[@id=\"securityGroups\"]"))
        	);
		List<WebElement> dropdown2 = selectsecurity.getOptions();
		Thread.sleep(3000);
		selectsecurity.selectByValue("sg-1a38d97f");
		  
		//Select Network
		WebElement network = webDriver.findElement(By.xpath("//select[@id=\"network\"]")); 
        Select selectnetwork= new Select(network);
        wait.until(
        	    ExpectedConditions
        	        .presenceOfNestedElementsLocatedBy(By.xpath("//select[@id=\"network\"]"), By.tagName("option"))
        	);
        wait.until(
        	    ExpectedConditions
        	        .elementToBeClickable(By.xpath("//select[@id=\"network\"]"))
        	);
		List<WebElement> dropdown4 = selectnetwork.getOptions();
		Thread.sleep(3000);
		selectnetwork.selectByValue("subnet-7681a130");
		 
		//select Role
		WebElement role = webDriver.findElement(By.xpath("//select[@id=\"instanceRole\"]")); 
        Select selectnetrole = new Select(role);
        wait.until(
        	    ExpectedConditions
        	        .presenceOfNestedElementsLocatedBy(By.xpath("//select[@id=\"instanceRole\"]"), By.tagName("option"))
        	);
        wait.until(
        	    ExpectedConditions
        	        .elementToBeClickable(By.xpath("//select[@id=\"instanceRole\"]"))
        	);
		List<WebElement> dropdown5 = selectnetrole.getOptions();
		Thread.sleep(3000);
		selectnetrole.selectByValue("aws-k8s-role");
		
		webDriver.findElement(By.xpath("//button[@class='btn btn-next btn-nirmata btn-default btn-primary']")).click();
    	 
    	 
		}
		catch(Throwable e){
		    System.out.println("Error found: "+e.getMessage());
		}
	}
 
	@Test(priority=11,dependsOnMethods= {"fillingSettingTabData"},testName= "Set the data in Host Count tab")
	public void fillingHostcountTabData()
	{
		//methodInfo.info("Taking Default data only.");
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		   wait.until(
	        	    ExpectedConditions
	        	        .elementToBeClickable(By.xpath("//*[@class=\"btn btn-next btn-nirmata btn-default btn-primary\"]"))
	        	);
		WebElement userData = webDriver.findElement(By.xpath("//*[@class=\"btn btn-next btn-nirmata btn-default btn-primary\"]")); 
		userData.click();
	}
	
	@Test(priority=12,dependsOnMethods= {"fillingHostcountTabData"},testName= "Set the data in User Data tab")
	public void fillingUserDataTabData() throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		WebElement userData = webDriver.findElement(By.xpath("//select[@id=\"installSelect\"]")); 
        Select selectUserData = new Select(userData);
        wait.until(
        	    ExpectedConditions
        	        .presenceOfNestedElementsLocatedBy(By.xpath("//select[@id=\"installSelect\"]"), By.tagName("option"))
        	);
        wait.until(
        	    ExpectedConditions
        	        .elementToBeClickable(By.xpath("//select[@id=\"installSelect\"]"))
        	);
		List<WebElement> dropdown6 = selectUserData.getOptions();
		Thread.sleep(3000);
		selectUserData.selectByIndex(4);
		 
		WebElement FinishButton = webDriver.findElement(By.xpath("//button[@class='btn btn-finish btn-nirmata btn-default btn-primary']"));
		FinishButton.click();
		wait.until(ExpectedConditions.visibilityOf(webDriver.findElement(By.xpath("//span[@class='bootstrap-dialog-button-icon fa fa-circle-o-notch fa-spin icon-spin']"))));
	    webDriver.navigate().refresh();  
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));
	}
	

	@Test(priority=13,dependsOnMethods= {"fillingUserDataTabData"},testName= "Click on Host Group Menu")
	public void clickHostGroupMenu() throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(webDriver, 360);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("hostgroups_menu")));
		
		WebElement HomeGroupMenu=webDriver.findElement(By.id("hostgroups_menu"));
		HomeGroupMenu.click();
		
	}
	
	@Test(priority=14,dependsOnMethods= {"clickHostGroupMenu"},testName= "Click on Amazon Web Services Menu")
	public void clickOnAwsHostGroupButton()
	{
		WebElement MicrosoftAzurebutton=webDriver.findElement(By.id("aws_menu"));
		MicrosoftAzurebutton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 360);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));
		
		WebElement awsPage = webDriver.findElement(By.id("model-content-panel-title"));
		String awsPage_title= awsPage.getText();
		Assert.assertEquals(awsPage_title, appproperties.properties.getProperty("awsPageTitle"));
	}
	
	
	@Test(priority=15,dependsOnMethods= {"clickOnAwsHostGroupButton"},testName= "check status")
	@Parameters("HostGroupName")
	public void checkStatus(String HostGroupName) throws InterruptedException
	{ 
		WebDriverWait wait = new WebDriverWait(webDriver, 360);
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(), '"+HostGroupName+"')]")));
		WebElement clickCreatedCard = webDriver.findElement(By.xpath("//span[contains(text(), '"+HostGroupName+"')]"));
		clickCreatedCard.click();
		 
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='status-badge-lg status-green']")));
		int count = 0;
		while(count<2) {
			
			webDriver.navigate().refresh();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='status-badge-lg status-green']")));
			WebElement checkStatus = webDriver.findElement(By.xpath("//span[@class='status-badge-lg status-green']"));
			//System.out.println("hgStatus"+checkStatus.getText());
			if(checkStatus.getText().equals("Connected")) {
				count++;
				System.out.println("hgStatus"+checkStatus.getText());
			}else {
				Assert.fail("Hostgroup connect : "+checkStatus.getText());
			}
		}
		
	}
}
