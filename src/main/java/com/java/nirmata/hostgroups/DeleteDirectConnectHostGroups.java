package com.java.nirmata.hostgroups;

import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;
import com.java.nirmata.hostgroups.RunSystemScripts;


public class DeleteDirectConnectHostGroups {
	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	@BeforeClass
	public void setupDriver()
	{
		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	
	@Test(priority=6, testName= "Clean Up Host")
	public void cleanUpHost() throws IOException
	{
		new RunSystemScripts().runExec("sudo docker stop $(sudo docker ps | grep 'calico' | gawk '{print $1}')");
		new RunSystemScripts().runExec("sudo docker stop $(sudo docker ps | grep 'flannel' | gawk '{print $1}')");	
		new RunSystemScripts().runExec("sudo docker stop $(sudo docker ps | grep 'nirmata' | gawk '{print $1}')");	
		new RunSystemScripts().runExec("sudo docker stop $(sudo docker ps | grep 'kube' | gawk '{print $1}')");	
		new RunSystemScripts().runExec("sudo docker rm  $(sudo docker ps -a | grep 'Exit' |gawk '{print $1}')");	
		new RunSystemScripts().runExec("sudo rm -rf /etc/cni/*");	
		new RunSystemScripts().runExec("sudo rm -rf /opt/cni/*");	
		new RunSystemScripts().runExec("sudo iptables --flush");	
		new RunSystemScripts().runExec("sudo iptables -tnat --flush");	
		new RunSystemScripts().runExec("sudo systemctl stop docker");	
		new RunSystemScripts().runExec("sudo ifconfig cni0 down");	
		new RunSystemScripts().runExec("sudo brctl delbr cni0");	
		new RunSystemScripts().runExec("sudo ip link delete cni0");
		new RunSystemScripts().runExec("sudo ip link delete flannel.1");	
		new RunSystemScripts().runExec("sudo rm -rf /data");	
		
	}
	
	@Test(priority=7,testName= "Click on Host Group Menu")
	public void clickHostGroup()
	{
		WebElement HomeGroupMenu=webDriver.findElement(By.id("hostgroups_menu"));
		HomeGroupMenu.click();
		
	}
	@Test(priority=8,dependsOnMethods= {"clickHostGroup"},testName= "Click on Direct Connect Menu")
	public void clickOnDirectConnect()
	{
		WebElement DirectConnectbutton=webDriver.findElement(By.id("other_host_groups_menu"));
		DirectConnectbutton.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("model-content-panel-title")));
		
		WebElement DirectConnectPage = webDriver.findElement(By.id("model-content-panel-title"));
		String DirectConnectPage_title= DirectConnectPage.getText();
		Assert.assertEquals(DirectConnectPage_title, appproperties.properties.getProperty("DirectConnect"));
	}
	@Test(priority=9,dependsOnMethods= {"clickOnDirectConnect"},testName= "Host Group Details")
	public void clickForHostGroupDetails()
	{
		
		WebElement SpecificHostGroup=webDriver.findElement(By.xpath("//*[contains(text(), '"+appproperties.properties.getProperty("HostGroupName")+"')]"));
		SpecificHostGroup.click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("state-popover")));
	}
	@Test(priority=10,testName= "Click on Setting Button",dependsOnMethods= {"clickForHostGroupDetails"})
	public void clickOnSettingButton()
	{
		WebElement SettingButton=webDriver.findElement(By.id("model-action-button"));
		SettingButton.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("deleteHostGroup")));
	}
	@Test(priority=11,testName= "Click on Delete Button",dependsOnMethods= {"clickOnSettingButton"})
	public void clickOnDeleteButton()
	{
		WebElement DeleteButton=webDriver.findElement(By.id("deleteHostGroup"));
		DeleteButton.click();
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='btn btn-danger']")));
	}
	@Test(priority=12,testName= "Delete Cluster",dependsOnMethods= {"clickOnDeleteButton"})
	public void deleteCluster()
	{
		webDriver.findElement(By.xpath("//button[@class='btn btn-danger']")).click();
		
	}
}
