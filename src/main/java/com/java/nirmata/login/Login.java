package com.java.nirmata.login;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.java.nirmata.HelperClass;
import com.java.nirmata.NirmataApplicationProperties;
import com.java.nirmata.NirmataSetUp;

public class Login{

	private NirmataApplicationProperties  appproperties;
	private WebDriver webDriver;
	@BeforeClass
	public void SetupDriver()
	{

		this.webDriver=NirmataSetUp.webDriver;
		this.appproperties=NirmataSetUp.appproperties;
	}
	
	@Test(priority=1,testName= "Open Login Page")
	@Parameters("url")
	public void openLoginPage(String url)
	{
		webDriver.get(url);
		String title= webDriver.getTitle();
		Assert.assertEquals(title, appproperties.properties.getProperty("LoginPageTitle"));

	}
	@Test(dependsOnMethods= {"openLoginPage"},priority=2,testName= "Verify Login Page")
	public void verifyLoginPage()
	{
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("login-title")));
		
		WebElement signinpage = webDriver.findElement(By.className("login-title"));
		String loginPagetitel= signinpage.getText();
		Assert.assertEquals(loginPagetitel, appproperties.properties.getProperty("SigninPageHeader"));
		
		
	}
	@Test(dependsOnMethods= {"verifyLoginPage"},priority=3,testName= "Set Email Id")
	@Parameters("email")
	public void setEmailID(String email) throws InterruptedException
	{
		
		webDriver.findElement(By.id("email")).sendKeys(email);
		webDriver.findElement(By.xpath("//button[@id='btnLogin']")).click();
		
		WebDriverWait wait = new WebDriverWait(webDriver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("email-address")));
	}
	
	@Test(dependsOnMethods= {"setEmailID"},priority=4,testName= "Verifying Password Page")
	@Parameters("accountno")
	public void verifyPasswoardPage(String accountno) throws IOException, InterruptedException
	{
	
		WebElement pageEmail = webDriver.findElement(By.className("login-title"));
		String PageTitle= pageEmail.getText();
		
		//System.out.println("Page Title : "+PageTitle);
		
		if(PageTitle.contains(appproperties.properties.getProperty("IdentitiMessege")))
		{
			NirmataSetUp.methodInfo.info("Identiti page");
			boolean status=true;
			
			Date currentDate=new Date();
			do
			{
				HelperClass helpclass=new HelperClass();
				String code=helpclass.getCode();
				WebElement pageIdentity = webDriver.findElement(By.className("login-title"));
				String IdentityTitle= pageIdentity.getText();
				if(code!=null && IdentityTitle.contains(appproperties.properties.getProperty("IdentitiMessege")))
				{
					webDriver.findElement(By.id("password")).clear();
					webDriver.findElement(By.id("password")).sendKeys(code);
					webDriver.findElement(By.xpath("//button[@id='btnLogin']")).click();
					
				}
				
				if(webDriver.findElements(By.id("accounts")).size()!=0)
				{
					status=false;
					WebDriverWait wait = new WebDriverWait(webDriver, 40);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("accounts")));
				}
				else if(webDriver.findElements(By.id("passwordForm")).size()!=0)
				{
					status=false;
					WebDriverWait wait = new WebDriverWait(webDriver, 40);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("passwordForm")));
				}
				long diff =  new Date().getTime()  - currentDate.getTime();
				long diffMinutes = diff / (60 * 1000) % 60;
				//System.out.println("Wait Time Verification Code :"+diffMinutes);
				if(diffMinutes>10)
				{
					Assert.fail("Verification Failed.");
					status=false;
				}
				Thread.sleep(5000);
					
			}while(status);
			
			
		}
		
		WebElement pageIdentity = webDriver.findElement(By.className("login-title"));
		PageTitle= pageIdentity.getText();
		//System.out.println("Page Title : "+PageTitle);
		
		if(PageTitle.contains(appproperties.properties.getProperty("AccountSelection")))
		{
			WebElement selectAccount=webDriver.findElement(By.xpath("//*[@id=\"accounts\"]/div["+accountno+"]"));
			selectAccount.click();
			boolean status=true;
			do
			{
				if(webDriver.findElements(By.id("passwordForm")).size()!=0)
				{
					status=false;
					WebDriverWait wait = new WebDriverWait(webDriver, 40);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("passwordForm")));
				}
				
			}while(status);
		}
		
		
		WebElement pagePassword = webDriver.findElement(By.className("login-title"));
		PageTitle= pagePassword.getText();
		//System.out.println("Page Title : "+PageTitle);
		
		if(PageTitle.contains(appproperties.properties.getProperty("PasswordPageHeading")))
		{
			NirmataSetUp.methodInfo.info("Password page");
			
		}
		else
		{
			Assert.fail("Not Able to find out Specific Page.");
		}
	}
	
	@Test(dependsOnMethods= {"verifyPasswoardPage"},priority=5, testName= "Set Password")
	@Parameters("password")
	public void setPassword(String password)
	{
		
			
			webDriver.findElement(By.id("password")).sendKeys(password);
			webDriver.findElement(By.xpath("//button[@id='btnLogin']")).click();
			
			boolean status=true;
			do
			{
				if(webDriver.findElements(By.id("skipSetup")).size()!=0)
				{
					status=false;
					webDriver.findElement(By.id("skipSetup")).click();
					WebDriverWait wait = new WebDriverWait(webDriver, 40);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("main-navigation")));
				}
				else if(webDriver.findElements(By.id("main-navigation")).size()!=0)
				{
					status=false;
					WebDriverWait wait = new WebDriverWait(webDriver, 40);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("main-navigation")));
				}
				
			}while(status);
			
			
		
	}
	
	

	
}
